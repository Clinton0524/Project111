import React from 'react'

export const Posts = ({posts, loading}) => {
    if (loading){
        return<h2> Loading</h2>;
    }
  return (
    <ul className="List-group mb-4">
        {posts.map(post => (
            <li key ={post.id} className='List-group-item'>
            {post.title}
            </li>
        ))}
        </ul>
  );
};
export default Posts;
