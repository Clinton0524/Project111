export const MenuItems =[
    {
        title:'Home',
        url:'/',
        cName:'nav-links'
    },
    {
        title:'Services',
        url:'/Services',
        cName:'nav-links'
    },
    {
        title:'Products',
        url:'/Product',
        cName:'nav-links'
    },
    {
        title:'Contact Us',
        url:'/Contact',
        cName:'nav-links'
    },
    {
        title:'Sign Up',
        url:'/Sign',
        cName:'nav-links-mobile'
    }
]