import React from "react";
import Products from "../Pages/Products"
import "../Pages/Products.css"


const Product = () => {
  console.log(Products);
  
  const listItems = Products.map((item) => 
    <div className="card" key={item.id}>
      <div className="card_img">
        <img src={item.thumb} alt="Girl in a jacket"  width="300" height="300" />
      </div>
      <div className="card_header">
        <h2>{item.product_name}</h2>
        <p>{item.description}</p>
        <p className="price">
          {item.price}
          <span>{item.Currency}</span>
        </p>
      </div>
    </div>
   
  );
  return (
    <div className="Product_content">
      <h3 className="Title">Headphones</h3>
      {listItems}
    </div>
  )


}
export default Product;
